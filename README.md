# windDrivenRainFoam

An open-source solver for wind-driven rain based on OpenFOAM

> **⚠️ Information**
>
> The repository for the solver is moved to GitHub. Future updates will be at:
>
> https://github.com/OpenFOAM-BuildingPhysics/windDrivenRainFoam
>
> **⚠️ Information**

<img src="https://carmeliet.ethz.ch/research/downloads/winddrivenrainfoam/_jcr_content/par/fullwidthimage_0/image.imageformat.fullwidth.1739434017.png"  width="60%">

More information at the Chair of Building Physics: https://carmeliet.ethz.ch
